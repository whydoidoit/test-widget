"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.render = render;
Object.defineProperty(exports, "flushEffects", {
  enumerable: true,
  get: function get() {
    return _reactTestingLibrary.flushEffects;
  }
});
Object.defineProperty(exports, "fireEvent", {
  enumerable: true,
  get: function get() {
    return _reactTestingLibrary.fireEvent;
  }
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _reactTestingLibrary = require("react-testing-library");

var _reactIocWidgets = require("react-ioc-widgets");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function render() {
  var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
      modules = _ref.modules,
      _ref$document = _ref.document,
      document = _ref$document === void 0 ? {} : _ref$document,
      _ref$editMode = _ref.editMode,
      editMode = _ref$editMode === void 0 ? false : _ref$editMode,
      _ref$bag = _ref.bag,
      bag = _ref$bag === void 0 ? {} : _ref$bag,
      _ref$design = _ref.design,
      design = _ref$design === void 0 ? {} : _ref$design,
      props = _objectWithoutProperties(_ref, ["modules", "document", "editMode", "bag", "design"]);

  var widgets;

  if (modules) {
    widgets = (0, _reactIocWidgets.widgetEventsFactory)();
    modules.forEach(function (init) {
      return init(widgets);
    });
  } else {
    widgets = _reactIocWidgets.globalWidgets;
  }

  var Component = function TestComponent(props) {
    return _react.default.createElement(_reactIocWidgets.Widgets, _extends({
      id: "test"
    }, props, {
      bag: bag,
      widgets: widgets,
      document: document,
      design: design,
      editMode: editMode,
      editable: true
    }), _react.default.createElement(_reactIocWidgets.EditorFrame, null));
  };

  var result = _objectSpread({}, (0, _reactTestingLibrary.render)(_react.default.createElement(Component, props)), {
    Component: Component,
    setEditMode: function setEditMode(value) {
      var currentEdit = !!result.queryByTestId("exitedit");

      if (value !== currentEdit) {
        if (value) {
          _reactTestingLibrary.fireEvent.click(result.getByTestId("enteredit"));
        } else {
          _reactTestingLibrary.fireEvent.click(result.getByTestId("exitedit"));
        }

        (0, _reactTestingLibrary.flushEffects)();
      }
    },
    selectEditorTab: function selectEditorTab(tab) {
      result.setEditMode(true);

      _reactTestingLibrary.fireEvent.click(result.getByTestId("editor-tab-".concat(tab)));

      (0, _reactTestingLibrary.flushEffects)();
    },
    document: document,
    design: design,
    bag: bag,
    widgets: widgets
  });

  (0, _reactTestingLibrary.flushEffects)();
  return result;
}

var _default = render;
exports.default = _default;