import testModule from "./example-module"
import {render, fireEvent} from "./render"

describe("Example Module", ()=>{
    it("Should render into the document", ()=>{
        const {getByText} = render({modules:[testModule], design: {title: "test"}, type: "test"})
        getByText("test")
    })
    it("should have an editor which updates the rendering", ()=>{
        const {getByText, setEditMode, getByTestId, selectEditorTab, asFragment} = render({modules: [testModule], design: {title: "testing"}, type: "test"})
        selectEditorTab("general")
        fireEvent.change(getByTestId("input"), {target: {value: "updated"}})
        setEditMode(false)
        expect(getByText("updated").nodeName).toBe("DIV")
        expect(asFragment()).toMatchSnapshot()
    })
})
