import React from "react"
import {useLayout, useDesign, useTabs} from "react-ioc-widgets"

function TestRender() {
    const [design] = useDesign()
    return <div>{design.title}</div>
}

function TestEditor() {
    const [design, update] = useDesign()
    return <input data-testid="input" value={design.title} onChange={e=>update({title: e.target.value})}/>
}


export default function (widgets) {
    widgets.configure("test", ()=>{
        useLayout({content: [TestRender]})
    })
    widgets.editor("test", ()=>{
        useTabs(TestEditor)
    })
}
