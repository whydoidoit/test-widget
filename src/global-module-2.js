import React from "react"
import {globalWidgets, useLayout} from "react-ioc-widgets"

function TestRenderer() {
    return <div>Test Global 2</div>
}

globalWidgets.configure("test", function () {
    useLayout({content: [TestRenderer]})
})
