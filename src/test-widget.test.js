import {render} from "./render"
import {fireEvent, flushEffects} from "react-testing-library"

describe("Test Widget", function() {
    it("should not crash when rendering", function() {
        const {asFragment, getByTestId} = render()
        expect(asFragment).not.toBe(null)
    })
    it("should be able to enter edit mode with an event or a call", function () {
        const {setEditMode, getByTestId} = render()
        setEditMode(true)
        let editButton = getByTestId("exitedit")
        fireEvent.click(editButton)
        getByTestId("enteredit")
        setEditMode(true)
        getByTestId("exitedit")
        setEditMode(false)
        getByTestId("enteredit")
    })
})
