import {render} from "./render"
import "./global-module-2"

it("should be able to render global widgets", function () {
    const {getByText, queryByText} = render({type: "test"})
    getByText("Test Global 2")
    expect(()=>getByText("Test Global 1")).toThrow()
})
