import React, {useState} from "react"
import {flushEffects, render as renderTest, fireEvent} from "react-testing-library"
import {EditorFrame, widgetEventsFactory, Widgets, globalWidgets} from "react-ioc-widgets"

export {fireEvent, flushEffects}

function render({modules, document = {}, editMode = false, bag={}, design = {}, ...props}={}) {
    let widgets

    if(modules) {
        widgets = widgetEventsFactory()
        modules.forEach(init => init(widgets))
    } else {
        widgets = globalWidgets
    }
    let Component = function TestComponent(props) {
        return <Widgets id="test" {...props} bag={bag} widgets={widgets} document={document} design={design} editMode={editMode} editable={true}>
            <EditorFrame/>
        </Widgets>
    }
    let result = {
        ...renderTest(<Component {...props}/>),
        Component,
        setEditMode(value) {
            let currentEdit = !!result.queryByTestId("exitedit")
            if(value !== currentEdit) {
                if(value) {
                    fireEvent.click(result.getByTestId("enteredit"))
                } else {
                    fireEvent.click(result.getByTestId("exitedit"))
                }
                flushEffects()
            }
        },
        selectEditorTab(tab) {
            result.setEditMode(true)
            fireEvent.click(result.getByTestId(`editor-tab-${tab}`))
            flushEffects()
        },
        document,
        design,
        bag,
        widgets
        }
    flushEffects()
    return result
}

export {render}
export default render
