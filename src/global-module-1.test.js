import {render} from "./render"
import "./global-module-1"

it("should be able to render global widgets", function() {
    const {getByText} = render({type: "test"})
    getByText("Test Global 1")
})
